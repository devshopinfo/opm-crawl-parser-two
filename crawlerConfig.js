module.exports = {
  maxDepth: 2,
  // interval: 1500,
  maxConcurrency: 3,
  fetchFileTypesWhiteList: [
    /^([^.]+)$/,
    /\.html?$/i,
    /\.jpe?g$/i,
    /\.png$/i,
    /\.css$/i,
    /\.js$/i,
    /\.pdf$/i,
    /\.docx?$/i,
    /\.xlsx?$/i,
    /\.aspx?$/i,
    /\.txt$/i
  ]
}
