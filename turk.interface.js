console.log('Entered: MechanicalTurk interface')

$(() => {
  console.log('Document ready')
  $('body').append('')
  $('*[data-opm-marker-id]')
    .css('border', '4px red solid')
    .on('hover', function (element) {
      console.log('%c' + $(this).attr('data-opm-marker-id'), 'color: #ff00ff')
    })
})

function assembleQuestions() {
  
}

function getData() {
  var markerIds = []

  $('*[data-opm-marker-id]').each(function () {
    markerIds.push($(this).attr('data-opm-marker-id'))
  })

  $.ajax({
    url: '/crawl-parser-api',
    contentType: 'application/json',
    cache: false,
    dataType: 'json',
    type: 'POST',
    data: markerIds,
    error: function (xhr, status, error) {
      console.log(error)
    },
    success: function (data) {
      console.log('getData: entered ajax success')

      // parse the data object, set any processing flags based on what data (if any) was returned
      parseData(data)

    }
  })
}

function parseData(data) {

}
