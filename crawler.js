'use strict'

const _ = require('lodash')
const chalk = require('chalk')
const Crawler = require('simplecrawler')
const crypto = require('crypto');
const moment = require('moment')
const url = require('url')
const fs = require('fs')
const Fuse = require('fuse.js')

const crawlerConfig = require('./crawlerConfig')
const db = require('./db')
const s3 = require('./aws')

module.exports = function (siteUrl, additionalAttempt) {
  return new Promise(async (resolve, reject) => {
    console.log(chalk.bgMagenta('ENTER:') + '\tcrawler main function, site = ' + chalk.blue(siteUrl))
    const crawler = Object.assign(new Crawler(siteUrl), crawlerConfig)

    const fileTypeLookups = {
      css: 'text/css',
      js: 'text/javascript',
      html: 'text/html',
      jpg: 'image/jpeg',
      png: 'image/png'
    }

    const siteMd5Hashes = []

    const siteObject = {
      siteUrl: siteUrl,
      bucket_name: url.parse(siteUrl).hostname.replace('www.', '').replace('.', '-'),
      do_not_upload: false,
      paths: [],
      visited_href: [],
      school_title_candidates: [],
      summary_candidates: [],
      meta_keyword_candidates: [],
      menu_candidates: [],
      phone_candidates: [],
      email_candidates: [],
      address_candidates: [],
      social_media_link_candidates: [],
      open_graph_candidates: [],
      image_candidates: [],
      incomplete_image_candidates: [],
      program_candidates: [],
      logo_image_candidates: []
    }

    const parser = require('./parser')(siteObject)

    // For every link, the crawler's fetch engine will utilize this regex to determine whether to crawl the url or not.
    crawler.addFetchCondition(supportedFetchFileTypes)

    crawler.on('crawlstart', async function () {
      console.log(chalk.bgBlack('START:') + '\tcrawling site = ' + chalk.blue(siteUrl))
    })

    crawler.on('fetchcomplete', async function (queueItem, responseBuffer, response) {
      if (siteObject.do_not_upload) return
      // Prevent complete event from being emitted
      const continueCrawl = this.wait()

      if (!additionalAttempt && queueItem.path.includes('wp-content')) {
        console.log(chalk.bgBlack('ENDED:') + '\tcrawling due to WordPress reroute site = ' + chalk.blue(siteUrl))
        continueCrawl()
        siteObject.do_not_upload = true
        crawler.emit('complete')
        resolve({
          exception: 'wp_reroute',
          new_route: queueItem.url.substring(0, queueItem.url.indexOf('wp-content'))
        })
      }

      console.log('\tI just received url %s, path %s (%d bytes)', chalk.blue(queueItem.url), chalk.cyan(queueItem.path), responseBuffer.length)
      console.log('\tIt was a resource of type %s', chalk.magenta(response.headers['content-type']))

      // resource has already been parsed and cached in S3, skip this fetch

      try {
        if (response.headers['content-type'].includes('text/html')) {

          if (isDuplicateResource(responseBuffer.toString())) {
            throw Error(`page is a duplicate resource. site = ${queueItem.url}`)
          }

          let htmlString = responseBuffer.toString()

          if (queueItem.url === siteUrl || queueItem.path.includes('index.html')) {
            siteObject.paths.root = queueItem.path
            htmlString = parser.parseRoot(queueItem.path, htmlString)
          }

          if (queueItem.path.includes('about')) {
            siteObject.paths.about_us = queueItem.path
            htmlString = parser.parseAboutUs(queueItem.path, htmlString)
          }
          if (queueItem.path.includes('contact')) {
            siteObject.paths.contact = queueItem.path
            htmlString = parser.parseContacts(queueItem.path, htmlString)
          }

          htmlString = parser.parsePage(queueItem.path, htmlString, queueItem.url)
          await handleTextResponse(queueItem.path, htmlString, fileTypeLookups.html)
        } else if (response.headers['content-type'].includes('image')) {

          await handleImage(queueItem.path, responseBuffer)
          parser.parseImage(queueItem.url, responseBuffer)

        } else if (response.headers['content-type'].includes('javascript')) {

          await handleTextResponse(queueItem.path, responseBuffer.toString(), fileTypeLookups.js)

        } else if (response.headers['content-type'].includes('text/css')) {

          await handleTextResponse(queueItem.path, responseBuffer.toString(), fileTypeLookups.css)

        } else throw Error('not ready to handle that yet! site = ' + chalk.blue(queueItem.url))
      } catch (err) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err.stack))
      } finally {
        continueCrawl()
      }

    })

    crawler.on('complete', () => {
      console.log(chalk.magenta(`crawler complete, siteObject size = ${_.size(JSON.stringify(siteObject, null, 4))}, site = `) + chalk.blue(siteUrl))

      if (siteObject.do_not_upload) return console.log(chalk.bgYellow('\t\t\t\t\t\t\t\t'))

      const insertionObject = Object.assign({ summary: generateSummary() },
        _.cloneDeep(siteObject))

      insertCandidates(insertionObject)

      // insertCandidates(siteObject)

      console.log(chalk.bgBlack('ENDED:') + '\tcrawling site = ' + chalk.blue(siteUrl))
    })

    try {
      if (await s3.bucketExists({ bucket_name: siteObject.bucket_name })) {
        const emptyBucketResults = await s3.emptySiteBucket({bucket_name: siteObject.bucket_name})
        console.log(chalk.bgGreen('YAY:') + chalk.green(`\tsuccessful emptying of bucket, site = ${chalk.blue(siteUrl)}, data = ${chalk.blue(JSON.stringify(emptyBucketResults))}`))
      } else {
        const createBucketResults = await s3.createSiteBucket({bucket_name: siteObject.bucket_name})
        console.log(chalk.bgGreen('YAY:') + chalk.green(`\tsuccessful creation of bucket, site = ${chalk.blue(siteUrl)}, data = ${chalk.blue(JSON.stringify(createBucketResults))}`))
      }
    } catch (err) {
      console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err))
    }

    crawler.start()

    // FUNCTIONS
    async function handleTextResponse(siteUrl, responseBody, fileType) {
      const bucketName = siteObject.bucket_name
      console.log(chalk.yellow(bucketName))
      let fileName = siteUrl

      console.log(chalk.yellow(fileName))
      if (fileType === fileTypeLookups.html && !siteUrl.includes('.html')) {
        fileName = siteUrl + 'page.html'
      }

      console.log(chalk.yellow(fileName))

      try {
        const addFileResults = await s3.addTextFile({bucket_name: bucketName, s3_file_name: fileName, content: responseBody, file_type: fileType})
        console.log(chalk.bgGreen('YAY:') + chalk.green(`\tsuccessful addition of script, site = ${chalk.blue(fileName)}, data = ${chalk.blue(JSON.stringify(addFileResults))}`))

      } catch (err) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err))
      }
    }

    async function handleImage(siteUrl, responseBuffer) {
      const bucketName = siteObject.bucket_name
      console.log(chalk.yellow(bucketName))
      const fileName = siteUrl
      console.log(chalk.yellow(fileName))

      const fileType = fileName.match(/\.jpe?g$/i) ?
        fileTypeLookups.jpg : fileTypeLookups.png

      try {
        const addFileResults = await s3.addImageFile({ bucket_name: bucketName, s3_file_name: fileName, content: responseBuffer, file_type: fileType })
        console.log(chalk.bgGreen('YAY:') + chalk.green(`\tsuccessful addition of image, site = ${chalk.blue(siteUrl)}, fileType = ${chalk.cyan(fileType)}, data = ${chalk.blue(JSON.stringify(addFileResults))}`))

      } catch (err) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err))
      }
    }

    async function handleCss(siteUrl, cssString) {
      const bucketName = siteObject.bucket_name
      console.log(chalk.yellow(bucketName))
      const fileName = siteUrl
      console.log(chalk.yellow(fileName))

      try {
        const addFileResults = await s3.addTextFile({bucket_name: bucketName, s3_file_name: fileName, content: cssString, file_type: 'css'})
        console.log(chalk.bgGreen('YAY:') + chalk.green(`\tsuccessful addition of stylesheet, site = ${chalk.blue(siteUrl)}, data = ${chalk.blue(JSON.stringify(addFileResults))}`))

      } catch (err) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err))
      }
    }

    function supportedFetchFileTypes(parsedURL) {
      return crawler.fetchFileTypesWhiteList.some(fileType => parsedURL.path.match(fileType))
    }

    function createMarkerTypeMap(siteObject) {

      const markerTypeMap = []

      _.each(siteObject, (dataList, dataCategory) => {
        if (!dataCategory.includes('_candidates')) return

        const category = dataCategory.replace('_candidates', '')

        for (let dataItem of dataList) {

          markerTypeMap.push([
            dataItem.marker_id,
            null,
            category,
            moment().format()
          ])

          if (!Array.isArray(dataItem.content)) continue

          for (let childItem of dataItem.content) {

            markerTypeMap.push([
              childItem.marker_id,
              childItem.parent_marker_id,
              category + '-child',
              moment().format()
            ])

          }
        }
      })

      return markerTypeMap
    }

    function isDuplicateResource(resourceString) {
    // if no duplicate page is found, add resource to the record

      const resourceHash = crypto.createHash('md5')
        .update(resourceString)
        .digest('hex')

      for (let hash of siteMd5Hashes) {
        if (hash === resourceHash) return true
      }

      siteMd5Hashes.push(resourceHash)
      return false
    }

    function generateSummary() {
      console.log(chalk.bgMagenta('ENTER:') + '\tgenerateSummary')

      const summary = {
        school_title: {},
        description: {
          ref: '',
          text: ''
        },
        menu_links: [{
          ref: '',
          text: '',
          site_url: '',
          s3_url: ''
        }],
        programs: [],
        logo_image: {
          ref: '',
          caption: '',
          file_name: '',
          width: 0,
          height: 0,
          site_url: '',
          s3_url: ''
        },
        activity_images: [{
          ref: '',
          caption: '',
          file_name: '',
          width: 0,
          height: 0,
          site_url: '',
          s3_url: ''
        }],
        address: {
          ref: '',
          text: ''
        },
        phone: {
          ref: '',
          text: ''
        },
        email: {
          ref: '',
          text: ''
        },
        keywords: [],
        social_media_links: []
      }

      summary.school_title = getBestSchoolTitleCandidate()
      summary.description = getBestDescriptionCandidate()
      summary.menu_links = getBestMenuLinkCandidates()
      summary.programs = getBestProgramCandidates()
      summary.logo_image = getBestLogoImageCandidate()
      summary.activity_images = getBestActivityImageCandidates()
      summary.address = getBestAddressCandidate()
      summary.phone = getBestPhoneCandidate()
      summary.email = getBestEmailCandidate()
      summary.keywords = getBestKeywordCandidates()
      summary.open_graph = getBestOpenGraphCandidates()
      summary.social_media_links = getBestSocialMediaLinkCandidates()

      return summary
    }

    function getBestSchoolTitleCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestSchoolTitleCandidate'))

      const bestTitle = {
        ref: '',
        text: ''
      }

      if (!siteObject.school_title_candidates.length) return bestTitle

      const options = {
        shouldSort: true,
        tokenize: true,
        includeScore: true,
        threshold: 1,
        location: 0,
        distance: 100,
        maxPatternLength: 64,
        minMatchCharLength: 1,
        keys: ['content']
      }

      const fuse = new Fuse(siteObject.school_title_candidates, options)

      const domainName = siteObject.siteUrl
        .substr(0, siteObject.siteUrl.lastIndexOf('-'))

      const fuseResult = fuse.search(domainName)

      if (fuseResult.length) {

        const bestFuseResult = fuseResult[0].item

        bestTitle.ref = bestResult.marker_id
        bestTitle.text = bestResult.content

        return bestTitle

      }

      const headTitle = _.find(siteObject.school_title_candidates,
        candidate => candidate.element_type == 'title')

      if (headTitle) {
        bestTitle.ref = headTitle.marker_id
        bestTitle.text = headTitle.content
      }

      return bestTitle
    }

    function getBestDescriptionCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestDescriptionCandidate'))

      if (!siteObject.summary_candidates.length) return {}

      const aboutUsSummary = _.find(siteObject.summary_candidates,
        candidate => candidate.page_path === siteObject.paths.about_us) ||
        _.find(siteObject.summary_candidates,
          candidate => candidate.page_path !== siteObject.paths.root)

      return aboutUsSummary ? {
        ref: aboutUsSummary.marker_id,
        text: aboutUsSummary.content
      } : {
        ref: siteObject.summary_candidates[0].marker_id,
        text: siteObject.summary_candidates[0].content,
      }

    }

    function getBestMenuLinkCandidates() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestMenuLinkCandidates'))

      if (!siteObject.menu_candidates.length) return {}

      const menuLinks = []

      for (let candidate of siteObject.menu_candidates[0].content) {
        if (!candidate.content.text || candidate.content.href === '#') continue

        const menuLink = {
          ref: '',
          text: '',
          site_url: '',
          s3_url: ''
        }

        menuLink.ref = candidate.marker_id
        menuLink.text = candidate.content.text
        menuLink.site_url = candidate.content.href
        menuLink.s3_url = parser.getRelativeBucketLink(siteObject.bucket_name,
          menuLink.site_url)

        menuLinks.push(menuLink)
      }

      return menuLinks

    }

    function getBestProgramCandidates() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestProgramCandidates'))

      return {}
    }

    function getBestLogoImageCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestLogoImageCandidate'))

      if (!siteObject.image_candidates.length) return {}

      const logoImage = {
        ref: '',
        caption: '',
        file_name: '',
        width: 0,
        height: 0,
        site_url: '',
        s3_url: ''
      }

      const foundLogoImage = _.find(siteObject.image_candidates,
        candidate => candidate.type === 'logo')

      if (!foundLogoImage || !foundLogoImage.marker_id) return {}

      logoImage.ref = foundLogoImage.marker_id

      logoImage.caption = foundLogoImage.content.tag_alt ||
        foundLogoImage.content.tag_name ||
        foundLogoImage.content.tag_title

      logoImage.file_name = foundLogoImage.content.tag_src
        .substr(foundLogoImage.content.tag_src.lastIndexOf('/') + 1)

      logoImage.width = foundLogoImage.content.tag_width
      logoImage.height = foundLogoImage.content.tag_height

      logoImage.site_url = foundLogoImage.content.tag_src
      logoImage.s3_url = foundLogoImage.content.tag_src

      return logoImage
    }

    function getBestActivityImageCandidates() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestActivityImageCandidates'))

      if (!siteObject.image_candidates.length) return []

      const activityImages = []

      const foundActivityImages = _.filter(siteObject.image_candidates,
        candidate => candidate.type === 'activity')

      if (!foundActivityImages.length) return []

      console.log(chalk.bgYellow('CHECK:') + chalk.yellow('\tgetBestActivityImageCandidates'))
      let maxActivityImages = 0

      for (let image in foundActivityImages) {

        if (++maxActivityImages > 10) break

        if (!image || !image.marker_id) continue

        const activityImage = {}

        activityImage.ref = image.marker_id

        activityImage.caption = image.content.tag_alt ||
          image.content.tag_name ||
          image.content.tag_title

        activityImage.file_name = image.content.tag_src
          .substr(image.content.tag_src.lastIndexOf('/') + 1)

        activityImage.width = image.content.tag_width
        activityImage.height = image.content.tag_height

        activityImage.site_url = image.content.tag_src
        activityImage.s3 = image.content.tag_src

        activityImages.push(activityImage)
      }

      return activityImages
    }

    function getBestAddressCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestAddressCandidate'))

      if (!siteObject.address_candidates.length) return {}

      const address = {
        ref: '',
        text: ''
      }

      address.ref = siteObject.address_candidates[0].marker_id
      address.text = siteObject.address_candidates[0].content

      return address
    }

    function getBestPhoneCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestPhoneCandidate'))

      if (!siteObject.phone_candidates.length) return {}

      const phone = {
        ref: '',
        text: ''
      }

      phone.ref = siteObject.phone_candidates[0].marker_id
      phone.text = siteObject.phone_candidates[0].content

      return phone
    }

    function getBestEmailCandidate() {
      console.log(chalk.bgMagenta('ENTER:') + chalk.magenta('\tgetBestEmailCandidate'))

      if (!siteObject.email_candidates.length) return {}

      const email = {
        ref: '',
        text: ''
      }

      email.ref = siteObject.email_candidates[0].marker_id
      email.text = siteObject.email_candidates[0].content

      return email
    }

    function getBestKeywordCandidates() {
      if (!siteObject.meta_keyword_candidates.length) return []

      const candidate = siteObject.meta_keyword_candidates[0]

      return candidate.content.split(', ')
    }

    function getBestOpenGraphCandidates() {
      if (!siteObject.open_graph_candidates.length) return []

      const openGraphTags = []

      for (let candidate of siteObject.open_graph_candidates) {
        openGraphTags.push({
          ref: candidate.marker_id,
          property: candidate.content.tag_property,
          content: candidate.content.tag_content
        })
      }

      return openGraphTags
    }

    function getBestSocialMediaLinkCandidates() {
      if (!siteObject.email_candidates.length) return []

      const socialMediaLinks = []

      for (let candidate of siteObject.social_media_link_candidates) {
        socialMediaLinks.push({
          ref: candidate.marker_id,
          text: candidate.content.href
        })
      }

      return socialMediaLinks
    }


    function insertCandidates(parseResults) {
      console.log(chalk.green('\tentered insertCandidates, site = ') + chalk.blue(siteUrl))
      if (_.isEmpty(parseResults)) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\tno results taken from page, site = ') + chalk.blue(siteUrl))
        return
      }
      console.log(chalk.bgGreen('YAY:') + chalk.green('\tparseResults not empty!'))
      // for (let keyName of Object.keys(parseResults)) {
      //   if (!parseResults[keyName].length || keyName == 'do_not_upload') delete parseResults[keyName]
      //   else console.log(chalk.green('\tparseResults contains key = ') + chalk.cyan(keyName))
      // }


      console.log(chalk.magenta(`\tinserting siteObject size = ${_.size(JSON.stringify(parseResults, null, 4))}, site = `) + chalk.blue(siteUrl))

      let sql = ''
      sql += 'INSERT INTO parse_results SET ?; '
      // // OFF
      // sql += 'INSERT INTO marker_id_type (marker_id, parent_marker_id, category, date_created) VALUES ?;'

      let site_object_values = {
        host: siteUrl,

        // // OFF
        // site_object: JSON.stringify(parseResults),
        site_object: JSON.stringify(parseResults),
        date_created: moment().format()
      }

      // // OFF
      // let marker_type_map_values = createMarkerTypeMap(parseResults)



      db.query({
        sql: sql,
        // // OFF
        // values: [site_object_values, marker_type_map_values],
        values: [site_object_values],
        callback: (error, results, fields) => {
          if (error) {
            console.log(JSON.stringify(error, null, 4))
            return reject(error)
          } else console.log(chalk.bgGreen('SUCCESS:') + chalk.green('successful insertion of data, site = ') + chalk.blue(siteUrl))
          resolve(parseResults)
        }
      })

      // for testing without db
      // console.log(chalk.green(JSON.stringify(siteObject, null, 4)))
      // resolve(JSON.stringify(siteObject, null, 4))
    }
  })
}
