'use strict'

const AWS = require('aws-sdk')

// Load credentials and set region from JSON file
AWS.config.loadFromPath('./config.json')

module.exports = function () {

  const externals = {}

  externals.createSiteBucket = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const createParams = {
      Bucket: params.bucket_name
    }

    return s3.createBucket(createParams).promise()
  }

  externals.emptySiteBucket = async function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const listParams = { Bucket: params.bucket_name }

    const listObjectsResults = await s3.listObjects(listParams).promise()

    const emptyParams = {
      Bucket: params.bucket_name,
      Delete: {
        Objects: [],
        Quiet: false
      }
    }

    for (let object of listObjectsResults.Contents) {
      emptyParams.Delete.Objects.push({ Key: object.Key })
    }

    return s3.deleteObjects(emptyParams).promise()
  }

  externals.bucketExists = async function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const getParams = {
      Bucket: params.bucket_name
    }

    try {
      await s3.headBucket(getParams).promise()
      return true
    } catch (err) {
      if (err.code === 'NotFound') return false
    }
  }

  externals.addTextFile = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const putParams = {
      Bucket: params.bucket_name,
      Key: params.s3_file_name,
      ACL: 'private',
      Body: params.content,
      ContentType: params.file_type,
      Metadata: {
        type: params.file_type,
        site_id: params.site_id ? params.site_id : '0'
      },
      StorageClass: 'REDUCED_REDUNDANCY'
    }

    return s3.putObject(putParams).promise()
  }

  externals.addImageFile = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const putParams = {
      Bucket: params.bucket_name,
      Key: params.s3_file_name,
      ACL: 'public-read',
      Body: params.content,
      ContentType: params.file_type,
      Metadata: {
        type: params.file_type,
        site_id: params.site_id ? params.site_id : '0'
      }
      // StorageClass: 'REDUCED_REDUNDANCY'
    }

    return s3.putObject(putParams).promise()
  }



  externals.addJsFile = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const putParams = {
      Bucket: params.bucket_name,
      Key: params.s3_file_name,
      ACL: 'private',
      Body: params.js,
      Metadata: {
        site_id: params.site_id ? params.site_id : '0'
      },
      StorageClass: 'REDUCED_REDUNDANCY'
    }

    return s3.putObject(putParams).promise()
  }

  externals.addCssFile = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const putParams = {
      Bucket: params.bucket_name,
      Key: params.s3_file_name,
      ACL: 'private',
      Body: params.css,
      Metadata: {
        site_id: params.site_id ? params.site_id : '0'
      },
      StorageClass: 'REDUCED_REDUNDANCY'
    }

    return s3.putObject(putParams).promise()
  }

  externals.getSiteFile = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const getParams = {
      Bucket: params.bucket_name,
      Key: params.s3_file_name
    }

    return s3.getObject(getParams).promise()
  }

  externals.listBuckets = function () {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    return s3.listBuckets({}).promise()
  }

  externals.listObjects = function (params) {
    const s3 = new AWS.S3({
      region: 'us-east-1'
    })

    const listParams = { Bucket: params.bucket_name }

    return s3.listObjects(listParams).promise()
  }

  return externals

}()
