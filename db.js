'use strict'

const mysql = require('mysql')

module.exports = function () {

  const internals = {}
  const externals = {}

  const pool = mysql.createPool({
    connectionLimit: 30,
    host: 'opm-dev-1.cnanfdn5uc17.us-east-1.rds.amazonaws.com',
    user: 'ebfdf5aba84f4e23',
    password: 'b1e2c00f670a4cf5b286953569924d91',
    port: '3306',
    database: 'spider',
    multipleStatements: true,
    waitForConnections: true,
    queueLimit: 0
  })
  internals.pool = pool

  internals.connect = function (connectHandler) {
    pool.getConnection(function (err, connection) {
      if (err) return connectHandler(err, null)
      return connectHandler(null, connection)
    })
  }

  externals.query = function (params) {
    const sql = params.sql
    const values = params.values
    const queryHandler = params.callback
    internals.connect((err, connection) => {
      if (err) return queryHandler(err, null)
      connection.query(sql, values, (err, rows, fields) => {
        queryHandler(err, rows)
        connection.release()
      })
    })
  }

  return externals
}()
