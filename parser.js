'use strict'

const _ = require('lodash')
const chalk = require('chalk')
const cheerio = require('cheerio')
const fs = require('fs')
const request = require('request-promise-native')
const sizeOf = require('image-size')
const url = require('url')
const uuidV4 = require('uuid/v4')

module.exports = function (siteObject) {

  const internals = {}
  const externals = {}

  internals.imageCounter = 0
  internals.markerIdCounter = 0

  externals.parseRoot = function (pagePath, htmlString) {
    console.log(chalk.bgMagenta('ENTER:') + '\tparseRoot')

    // object containing all potentially useful info on root page
    const results = {}

    const $ = cheerio.load(htmlString, { normalizeWhitespace: true })

    // find all potential titles for this Mat's school/organization
    $('head > title').each((i, element) => {
      const relevantContent = removeWhitespace($(element).text())

      if (!relevantContent) return

      const markedElement = markElementAsRelevant($(element), 'school-title')
      const relevantMarkerId = markedElement.attr('data-opm-marker-id')
      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath,
        content: relevantContent,
        marker_id: relevantMarkerId
      }
      siteObject.school_title_candidates.push(relevantElement)
    })

    $('body h1').first().each((i, element) => {
      const relevantContent = removeWhitespace($(element).text())

      if (!relevantContent) return

      const markedElement = markElementAsRelevant($(element), 'school-title')
      const relevantMarkerId = markedElement.attr('data-opm-marker-id')
      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath,
        content: relevantContent,
        marker_id: relevantMarkerId
      }
      siteObject.school_title_candidates.push(relevantElement)
    })

    $('body p').first().each((i, element) => {
      const $element = $(element)
      const relevantContent = removeWhitespace($element.text())

      if (!relevantContent) return

      const markedElement = markElementAsRelevant($element, 'summary')
      const relevantMarkerId = markedElement.attr('data-opm-marker-id')
      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath,
        content: relevantContent,
        marker_id: relevantMarkerId
      }
      $(element).replaceWith($.html(markedElement))
      siteObject.summary_candidates.push(relevantElement)
    })

    $('head > meta').each((i, element) => {
      const $meta = $(element)
      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath
      }

      let markedElement = $meta
      if ($meta.attr('name') === 'keywords') {
        markedElement = markElementAsRelevant($meta, 'meta-keyword')
        relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
        relevantElement.content = $meta.attr('content')

        siteObject.meta_keyword_candidates.push(relevantElement)

      } else if ($meta.attr('property') && $meta.attr('property').match(/^(og|fb|twitter):/i)) {
        markedElement = markElementAsRelevant($meta, 'open-graph')
        relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
        relevantElement.content = {
          tag_property: $meta.attr('property'),
          tag_content: $meta.attr('content')
        }

        siteObject.open_graph_candidates.push(relevantElement)
      }

      $(element).replaceWith($.html(markedElement))

    })

    $('body').find('*').each((i, element) => {
      const $element = $(element)
      const $elementClass = $element.attr('class') ? $element.attr('class').toLowerCase() : ''
      const $elementId = $element.attr('id') ? $element.attr('id').toLowerCase() : ''

      console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT AAA'))

      if (!$elementId.includes('menu') &&
        !$elementId.includes('nav') &&
        !$elementClass.includes('menu') &&
        !$elementClass.includes('nav')
      ) return // element is probably not a menu, return early

      // console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT BBB'))
      // console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(` : PRINTPOINT 2 = ${$elementClass}`))
      // console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(` : PRINTPOINT 3 = ${$elementId}`))

      const relevantElement = findMenuLinks($, $element)

      relevantElement.element_type = element.tagName

      if (!relevantElement.content) return

      console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT CCC'))

      relevantElement.page_path = pagePath
      siteObject.menu_candidates.push(relevantElement)

      // if maximum reached, return false to break out of cheerio's each loop
      if (siteObject.menu_candidates.length > 3) return false
    })

    $('footer').each((i, element) => {
      const $footer = $(element)

      $footer.find('*').each((i, element) => {
        const $element = $(element)

        const phoneNumber = searchForPhoneNumbers($element.text())
        const email = searchForEmails($element.text())



        const address = searchForAddresses(
          $element.contents()
            .map(function () {
              return (this.type === 'text') ? $(this).text() : ''
            })
            .get()
            .join(' ')
        )

        let markedElement = ''
        let relevantElement = {}

        if (phoneNumber) {
          markedElement = markElementAsRelevant($element, 'phone')
          relevantElement = {
            element_type: element.tagName,
            content: phoneNumber,
            marker_id: markedElement.attr('data-opm-marker-id'),
            page_path: pagePath
          }
          $(element).replaceWith($.html(markedElement))

          siteObject.phone_candidates.push(relevantElement)
        }

        if (email) {
          markedElement = markElementAsRelevant($element, 'email')
          relevantElement = {
            element_type: element.tagName,
            content: email,
            marker_id: markedElement.attr('data-opm-marker-id'),
            page_path: pagePath
          }
          $(element).replaceWith($.html(markedElement))

          siteObject.email_candidates.push(relevantElement)
        }

        if (address) {
          markedElement = markElementAsRelevant($element, 'address')
          relevantElement = {
            element_type: element.tagName,
            content: address,
            marker_id: markedElement.attr('data-opm-marker-id'),
            page_path: pagePath
          }
          $(element).replaceWith($.html(markedElement))

          siteObject.email_candidates.push(relevantElement)
        }

      })

      $footer.find('a').each((i, element) => {
        const $anchor = $(element)
        const relevantElement = {
          element_type: element.tagName,
          page_path: pagePath
        }

        if ($anchor.attr('href')) {
          relevantElement.content = { href: $anchor.attr('href') }

          if (relevantElement.content.href.includes('facebook')) relevantElement.content.type = 'facebook'
          if (relevantElement.content.href.includes('twitter')) relevantElement.content.type = 'facebook'
          if (relevantElement.content.href.includes('instagram')) relevantElement.content.type = 'instagram'
          if (relevantElement.content.href.includes('plus.google')) relevantElement.content.type = 'google plus'
          if (relevantElement.content.href.includes('pinterest')) relevantElement.content.type = 'pinterest'

          if (relevantElement.content.type) {
            const markedElement = markElementAsRelevant($anchor, 'social-media-link')
            relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
            $(element).replaceWith($.html(markedElement))

            siteObject.social_media_link_candidates.push(relevantElement)
          }
        }
      })
    })

    return $.html()
  }

  externals.parseContacts = function (pagePath, htmlString) {
    console.log(chalk.bgMagenta('ENTER:') + '\tparseContacts')

    const $ = cheerio.load(htmlString, { normalizeWhitespace: true })


    $('body').find('*').each((i, element) => {
      const $element = $(element)

      const phoneNumber = searchForPhoneNumbers($element.text())
      const email = searchForEmails($element.text())

      let markedElement = ''
      let relevantElement = {}

      if (phoneNumber) {
        markedElement = markElementAsRelevant($element, 'phone')
        relevantElement = {
          element_type: element.tagName,
          content: phoneNumber,
          marker_id: markedElement.attr('data-opm-marker-id'),
          page_path: pagePath
        }
        $(element).replaceWith($.html(markedElement))

        siteObject.phone_candidates.push(relevantElement)
      }

      if (email) {
        markedElement = markElementAsRelevant($element, 'email')
        relevantElement = {
          element_type: element.tagName,
          content: email,
          marker_id: markedElement.attr('data-opm-marker-id'),
          page_path: pagePath
        }
        $(element).replaceWith($.html(markedElement))

        siteObject.email_candidates.push(relevantElement)
      }
    })

    $('body a').each((i, element) => {
      const $element = $(element)

      if ($element.attr('href') && $element.attr('href').includes('tel:')) {

        const markedElement = markElementAsRelevant($element, 'phone')
        const relevantElement = {
          element_type: element.tagName,
          content: removeWhitespace($element.text()),
          marker_id: markedElement.attr('data-opm-marker-id'),
          page_path: pagePath
        }
        $(element).replaceWith($.html(markedElement))

        siteObject.phone_candidates.push(relevantElement)

      } else if ($element.attr('href') && $element.attr('href').includes('mailto:')) {

        const markedElement = markElementAsRelevant($element, 'email')
        const relevantElement = {
          element_type: element.tagName,
          content: removeWhitespace($element.text()),
          marker_id: markedElement.attr('data-opm-marker-id'),
          page_path: pagePath
        }
        $(element).replaceWith($.html(markedElement))

        siteObject.email_candidates.push(relevantElement)
      }
    })

    $('body address').each((i, element) => {
      const $element = $(element)

      const markedElement = markElementAsRelevant($element, 'email')
      const relevantElement = {
        element_type: element.tagName,
        content: removeWhitespace($element.text()),
        marker_id: markedElement.attr('data-opm-marker-id'),
        page_path: pagePath
      }
      $(element).replaceWith($.html(markedElement))

      siteObject.address_candidates.push(relevantElement)
    })

    return $.html()
  }

  externals.parseAboutUs = function (pagePath, htmlString) {
    console.log(chalk.bgMagenta('ENTER:') + '\tparseAboutUs')

    // object containing all potentially useful info on root page
    const results = {}

    const $ = cheerio.load(externals.parseContacts(pagePath, htmlString), { normalizeWhitespace: true })

    $('body p').first().each((i, element) => {
      const $element = $(element)

      const relevantContent = removeWhitespace($element.text())

      if (!relevantContent) return

      const markedElement = markElementAsRelevant($element, 'summary')
      const relevantMarkerId = markedElement.attr('data-opm-marker-id')
      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath,
        content: relevantContent,
        marker_id: relevantMarkerId
      }
      $(element).replaceWith($.html(markedElement))

      siteObject.summary_candidates.push(relevantElement)
    })

    return $.html()
  }

  externals.parsePage = function (pagePath, htmlString, queueItemUrl) {
    console.log(chalk.bgMagenta('ENTER:') + `\tparsePage, queueItemUrl = ${chalk.blue(queueItemUrl)}, pagePath = ${chalk.cyan(pagePath)} `)

    // object containing all potentially useful info on root page
    const results = {}

    const $ = cheerio.load(htmlString, { normalizeWhitespace: true })

    $('img').each((i, element) => {
      const $imageElement = $(element)

      const relevantElement = {
        element_type: element.tagName,
        page_path: pagePath,
        content: []
      }

      relevantElement.content.tag_src = $imageElement.attr('src')
      // tags will be discarded without a src to tie it to a real image
      if (!relevantElement.content.tag_src) return

      if ($imageElement.attr('name')) relevantElement.content.tag_name = $imageElement.attr('name')
      if ($imageElement.attr('alt')) relevantElement.content.tag_alt = $imageElement.attr('alt')
      if ($imageElement.attr('title')) relevantElement.content.tag_title = $imageElement.attr('title')
      if ($imageElement.attr('class')) relevantElement.content.css_class = $imageElement.attr('class')

      // existingCandidate has a tag_src that has already been
      // scraped and the image file's dimensions measured
      const existingCandidate = _.clone(
        _.find(siteObject.image_candidates,
          candidate => candidate.content.tag_src.includes(relevantElement.content.tag_src)))

      let markedElement = $imageElement

      if (existingCandidate) {
        // existingCandidate's image src was scraped, but it was
        // already marked and shouldn't be marked again
        if (existingCandidate.marker_id) return

        markedElement = markElementAsRelevant($imageElement, 'image')
        relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
        $(element).replaceWith($.html(markedElement))

        const updatedCandidate = Object.assign(relevantElement, existingCandidate)

        _.remove(siteObject.image_candidates,
          candidate => candidate.content.tag_src == existingCandidate.content.tag_src)

        siteObject.image_candidates.push(updatedCandidate)
        internals.imageCounter++
      } else {

        // existingIncompleteCandidate only has metadata, i.e. the original image
        // has not been scraped yet or cannot be pulled from host's server
        const existingIncompleteCandidate = _.find(
          siteObject.incomplete_image_candidates,
          candidate => candidate.content.tag_src.includes(relevantElement.content.tag_src))

        if (existingIncompleteCandidate) return

        console.log(chalk.cyan($imageElement.data('opm-marker-id')))
        markedElement = markElementAsRelevant($imageElement, 'image')
        relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
        $imageElement.replaceWith($.html(markedElement))

        console.log(chalk.cyan($imageElement.data('opm-marker-id')))
        // console.log(chalk.cyan(JSON.stringify(element)))

        siteObject.incomplete_image_candidates.push(relevantElement)
        console.log($.html(markedElement))
        if (++internals.imageCounter > 12) return false

      }
    })
    // console.log(chalk.cyan($.html()))

    // PROGRAMS
    // Approach 1: find link that has 'programs' or classes' in title, name, href
      // if href = #
        // look for sublinks
      // if href is url inside domain (includes domain name OR is a relative path)
        // request page and search it for program info
      //

    const programHints = ['classes', 'program']

    $('body').find('a').each((i, element) => {
      const $anchor = $(element)

      // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : CHECKPOINT AAA'))
      const relevantAttributes = anchorIsRelevant($, $anchor, programHints)

      if (!relevantAttributes) return
      // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : CHECKPOINT AAA'))
      // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : PRINTPOINT AAA, relevantAttributes = ') + JSON.stringify(relevantAttributes))
      if (!relevantAttributes.href || (relevantAttributes.href && relevantAttributes.href == '#')) {
        // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : CHECKPOINT BBB'))
        // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : PRINTPOINT BBB, relevantAttributes.href = ') + relevantAttributes.href)
        $anchor.find('a').each((i, subElement) => {
          const $subAnchor = $(subElement)
          // console.log('\t' + chalk.bgYellow('parsePage') + chalk.magenta(' : CHECKPOINT CCC'))

          const subRelevantAttributes = anchorIsRelevant($, $subAnchor, programHints)
          if (!subRelevantAttributes ||
            !subRelevantAttributes.href ||
            (relevantAttributes.href && relevantAttributes.href == '#')) return

          // console.log('\t' + chalk.bgYellow('parsePage') + chalk.magenta(' : PRINTPOINT CCC, relevantAttributes = ') + JSON.stringify(relevantAttributes))
          searchForPrograms(subRelevantAttributes.href)
        })
        // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : CHECKPOINT DDD'))
      } else {
        // console.log('\t' + chalk.bgMagenta('parsePage') + chalk.magenta(' : CHECKPOINT EEE'))
        searchForPrograms(relevantAttributes.href)
      }
    })


    return this.preparePageForBucket($.html())
  }

  externals.parseImage = function (imageUrl, imageBuffer) {
    console.log(chalk.bgMagenta('ENTER:') + '\tparseImage')
    let imageCandidate = { content: {} }

    const existingCandidate = _.clone(
      _.find(siteObject.image_candidates,
        candidate => candidate.content.tag_src == imageUrl))

    if (existingCandidate) return console.log(
      chalk.bgMagenta('SKIP:') +
      chalk.magenta(`\timage already exists, src = ${imageUrl}, site = `) +
      chalk.blue(siteObject.siteUrl))

    const dimensions = sizeOf(imageBuffer)

    imageCandidate.content.tag_src = imageUrl
    imageCandidate.content.tag_width = dimensions.width
    imageCandidate.content.tag_height = dimensions.height

    if (imageUrl.includes('logo')) imageCandidate.type = 'logo'
    else if (dimensions.width >= 800 && dimensions.height >= 600) imageCandidate.type = 'activity'

    const existingIncompleteCandidate = _.find(
      siteObject.incomplete_image_candidates,
      candidate => imageCandidate.content.tag_src.includes(candidate.content.tag_src))

    if (existingIncompleteCandidate) {
      imageCandidate = Object.assign(existingIncompleteCandidate, imageCandidate, { has_metadata: true })

      _.remove(
        siteObject.incomplete_image_candidates,
        candidate => candidate.content.tag_src == existingIncompleteCandidate.content.tag_src)
    }
    siteObject.image_candidates.push(imageCandidate)
  }

  externals.preparePageForBucket = function (htmlString) {
    // iterates through all links on page, if they are relative links then
    // change them so they will work on the bucket-cached version

    const $ = cheerio.load(htmlString, { normalizeWhitespace: true })

    $('a, link').each((i, element) => {
      const tagHref = $(element).attr('href')

      // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(a, link)') + chalk.magenta(' : CHECKPOINT AAA'))
      if (!tagHref || isAbsoluteLink(tagHref)) return
      else $(element).attr('href', getRelativeBucketLink(siteObject.bucket_name, tagHref))
      // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(a, link)') + chalk.magenta(' : CHECKPOINT BBB'))
    })

    $('img, script').each((i, element) => {
      const tagSrc = $(element).attr('src')

      // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(img, script)') + chalk.magenta(' : CHECKPOINT AAA'))
      if (!tagSrc || isAbsoluteLink(tagSrc) || tagSrc.includes('data:')) return
      else $(element).attr('src', getRelativeBucketLink(siteObject.bucket_name, tagSrc))
      // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(img, script)') + chalk.magenta(' : CHECKPOINT BBB'))
    })

    $('script').each((i, element) => {
      // Find inline scripts for initializing analytics and replace with comment

      const $element = $(element)

      // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(script)') + chalk.magenta(' : CHECKPOINT AAA'))
      if ($element.html().includes('google-analytics.com')) {
        // console.log('\t' + chalk.bgMagenta('preparePageForBucket -> $(script)') + chalk.magenta(' : CHECKPOINT BBB'))
        $element.html('// Removed block containing google analytics code')
      }
    })

    const interfaceScriptString = fs.readFileSync('./turk.interface.js', 'utf8')
    const interfaceHtmlString = fs.readFileSync('./turk.interface.html', 'utf8')
    $('head').append('<link rel="stylesheet" href="css/turk.interface.css">')
    $('body').append(interfaceHtmlString)
    $('script').last().after('<script>' + interfaceScriptString + '</script>')
    return $.html()
  }

  function anchorIsRelevant($, $anchor, hintsList) {
    // console.log('\t' + chalk.bgMagenta('anchorIsRelevant') + chalk.magenta(' : CHECKPOINT AAA'))
    const anchorAttributes = []
    const relevantAttributes = {}

    if ($anchor.attr('class')) anchorAttributes.push({ key: 'class', value: $anchor.attr('class') })
    if ($anchor.attr('id')) anchorAttributes.push({ key: 'id', value: $anchor.attr('id') })
    if ($anchor.attr('href')) anchorAttributes.push({ key: 'href', value: $anchor.attr('href') })

    for (let attr of anchorAttributes) {
      for (let hint of hintsList) {
        if (attr.value.includes(hint) && !relevantAttributes[attr.key]) {
          relevantAttributes[attr.key] = attr.value
          console.log('\t' + chalk.bgMagenta('anchorIsRelevant') + chalk.magenta(' : CHECKPOINT BBB'))
        }
        // console.log('\t' + chalk.bgMagenta('anchorIsRelevant') + chalk.magenta(' : CHECKPOINT CCC'))
      }
    }

    const _text = $anchor.text()

    $anchor.find('*').not('a').each((i, element) => {
      if (element.type !== 'text') return
      console.log('\t' + chalk.bgMagenta('anchorIsRelevant') + chalk.magenta(' : CHECKPOINT DDD'))
      const _subText = removeWhitespace($(element).text())
      for (let hint of hintsList) {
        if (_subText.toLowerCase().includes(hint)) relevantAttributes.text = _subText
      }
    })
    for (let hint of hintsList) {
      if (_text.toLowerCase().includes(hint)) {
        relevantAttributes.text = _text
        console.log('\t' + chalk.bgMagenta('anchorIsRelevant') + chalk.magenta(' : CHECKPOINT EEE'))
      }
    }
    if (!_.isEmpty(relevantAttributes)) {
      if ($anchor.attr('href')) relevantAttributes['href'] = $anchor.attr('href')
      return relevantAttributes
    } else return null
  }

  async function searchForPrograms(pageUrl, secondAttempt) {
    console.log(chalk.bgMagenta('ENTER:') + '\tsearchForPrograms, pageUrl = ' + pageUrl+ ', secondAttempt = ' + !!secondAttempt)
    let options = {
      uri: secondAttempt ? url.resolve(siteObject.siteUrl, pageUrl) : pageUrl,
      method: 'GET'
    }
    try {
      // console.log('\t' + chalk.bgMagenta('searchForPrograms') + chalk.magenta(' : CHECKPOINT AAA'))
      // console.log('\t' + chalk.bgMagenta('searchForPrograms') + chalk.magenta(' : PRINTPOINT AAA, options.uri = ') + chalk.cyan(JSON.stringify(options.uri)))

      if (siteObject.visited_href.find(url => url == options.uri)) return
      siteObject.visited_href.push(options.uri)

      // console.log('\t' + chalk.bgYellow('searchForPrograms') + chalk.magenta(' : CHECKPOINT BBB'))
      const htmlString = await request(options)

      const $ = cheerio.load(htmlString, { normalizeWhitespace: true })
      // console.log('\t' + chalk.bgMagenta('searchForPrograms') + chalk.magenta(' : CHECKPOINT CCC'))

      const programCandidate = {
        page_url: options.uri,
        text_nodes: []
      }
      $('body').find('*').each((i, element) => {
        if (element.type !== 'text') return
        // console.log('\t' + chalk.bgMagenta('searchForPrograms') + chalk.magenta(' : CHECKPOINT DDD'))

        const _text = removeWhitespace($(element).text())

        if (!_text.length) return
        else programCandidate.text_nodes.push(_text)
      })

      if (!programCandidate.text_nodes.length) {
        siteObject.program_candidates.push(programCandidate)
        // console.log('\t' + chalk.bgMagenta('searchForPrograms') + chalk.magenta(' : CHECKPOINT EEE'))
      } else return
    } catch (err) {
      console.log(chalk.bgRed('ERROR:') + '\t' + chalk.blue(pageUrl) + chalk.red('\t' + err))
      if (!secondAttempt) searchForPrograms(pageUrl, true)
    }
  }

  function findHeader($) {
    let $headers = $('header, nav')

    if ($headers.length == 0) $headers = $('div').filter((i, element) => {
      const $element = $(element)
      return $element.attr('class') &&
        $element.attr('class').includes('header')
    })

    return ($headers.length > 0) ? $headers.first() : null
  }

  // function findMenuCandidates($, $bodyElement, pagePath) {
  //   console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(` : PRINTPOINT 1 = ${$bodyElement.attr('class') + ' ' + $bodyElement.get(0).tagName}`))
  //
  //   $bodyElement.find('*').each((i, element) => {
  //     const $element = $(element)
  //     const $elementClass = $element.attr('class') ? $element.attr('class').toLowerCase() : ''
  //     const $elementId = $element.attr('id') ? $element.attr('id').toLowerCase() : ''
  //
  //     console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT AAA'))
  //
  //     if (!$elementId.includes('menu') &&
  //       !$elementId.includes('nav') &&
  //       !$elementClass.includes('menu') &&
  //       !$elementClass.includes('nav')
  //     ) return // element is probably not a menu, return early
  //
  //     console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT BBB'))
  //     console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(` : PRINTPOINT 2 = ${$elementClass}`))
  //     console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(` : PRINTPOINT 3 = ${$elementId}`))
  //
  //     const relevantElement = findMenuLinks($, $element, pagePath)
  //
  //     if (!relevantElement.content) return
  //
  //     console.log('\t' + chalk.bgMagenta('findMenuCandidates') + chalk.magenta(' : CHECKPOINT CCC'))
  //     siteObject.menu_candidates.push(relevantElement)
  //
  //     // if maximum reached, return false to break out of cheerio's each loop
  //     if (siteObject.menu_candidates.length > 3) return false
  //   })
  // }

  function findMenuLinks($, $menuElement, pagePath) {
    // Iterates through menuElement and finds all <a> tags and their text nodes

    // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(' : CHECKPOINT AAA'))
    let validMenuCandidate = false

    $menuElement.find('a').each((i, anchorElement) => {
      const $anchorElement = $(anchorElement)
      let _text = ''

      if ($anchorElement.text().length) {

        _text = removeWhitespace($anchorElement.text())
      } else {
        // find the first text node that descends from anchor in DOM
        _text = removeWhitespace($anchorElement.find('*')
          .filter((i, element) => element.type == 'text')
          .first().text())
      }

      if (_text.toLowerCase().includes('home') ||
        _text.toLowerCase().includes('about') ||
        _text.toLowerCase().includes('contact') ||
        _text.toLowerCase().includes('program') ||
        _text.toLowerCase().includes('instructor') ||
        _text.toLowerCase().includes('staff') ||
        _text.toLowerCase().includes('event') ||
        _text.toLowerCase().includes('schedule')
      ) {
        // console.log('\t' + chalk.bgYellow('findMenuLinks') + chalk.magenta(' : CHECKPOINT EEE'))
        validMenuCandidate = true
        return false // break out of loop
      }
    })

    // This menu candidate has no useful links, return early
    if (!validMenuCandidate) return {}

    const markedElement = markElementAsRelevant($menuElement, 'menu')
    const relevantMarkerId = markedElement.attr('data-opm-marker-id')
    $menuElement.replaceWith($.html(markedElement))

    const relevantElement = {
      element_type: markedElement.get(0).tagName,
      marker_id: relevantMarkerId,
      content: []
    }

    $menuElement.find('a').each((i, anchorElement) => {
      // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(' : CHECKPOINT BBB'))
      const $anchorElement = $(anchorElement)
      // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(' : PRINTPOINT BBB = ' + $.html($anchorElement)))

      const anchorHref = $anchorElement.attr('href')
      if (!anchorHref) return console.log(chalk.bgRed('A'))

      const markedAnchorElement = markElementAsRelevant($anchorElement, 'menu-child', relevantMarkerId)
      $(anchorElement).replaceWith($.html(markedAnchorElement))

      const anchorObject = {
        parent_marker_id: relevantMarkerId,
        marker_id: markedAnchorElement.attr('data-opm-marker-id'),
        content: {
          text: '',
          href: anchorHref
        }
      }
      // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(' : CHECKPOINT CCC'))

      if ($anchorElement.text().length) {

        anchorObject.content.text = removeWhitespace($anchorElement.text())
      } else {
        // find the first text node that descends from anchor in DOM
        anchorObject.content.text = removeWhitespace($anchorElement.find('*')
          .filter((i, element) => element.type == 'text')
          .first().text())
      }


      // if (!_text.length) return console.log(chalk.bgRed('B'))

      // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(' : CHECKPOINT DDD'))

      relevantElement.content.push(anchorObject)


      // console.log('\t' + chalk.bgMagenta('findMenuLinks') + chalk.magenta(` : PRINTPOINT EEE = ${_text + ' ' + validMenuCandidate}`))
    })

    return relevantElement
  }

  // function prepareImage($imageElement, pagePath, queueItemUrl) {
  //   console.log(chalk.green(`\timage passed inspection, src = ${$imageElement.attr('src')}, site = `) + chalk.blue(queueItemUrl))
  //
  //   const relevantElement = {
  //     page_path: pagePath
  //   }
  //
  //   if ($imageElement.attr('src')) return
  //
  //   relevantElement.content.tag_src = $imageElement.attr('src')
  //   if ($imageElement.attr('name')) relevantElement.content.tag_name = $imageElement.attr('name')
  //   if ($imageElement.attr('alt')) relevantElement.content.tag_alt = $imageElement.attr('alt')
  //   if ($imageElement.attr('title')) relevantElement.content.tag_title = $imageElement.attr('title')
  //   if ($imageElement.attr('class')) relevantElement.content.css_class = $imageElement.attr('class')
  //
  //   // existingCandidate satisfies the minimum of having dimensions verified
  //   // by a scrape of the original image source
  //   let existingCandidate = _.clone(
  //     _.find(siteObject.image_candidates,
  //       candidate => candidate.content.tag_src.includes(relevantElement.content.tag_src)))
  //
  //   if (existingCandidate) {
  //     // existingCandidate's image src was scraped, but it was already marked
  //     // and shouldn't be marked again
  //     if (existingCandidate.marker_id) return
  //
  //     const markedElement = markElementAsRelevant($imageElement, 'image')
  //     relevantElement.marker_id = markedElement.attr('data-opm-marker-id')
  //
  //     existingCandidate = Object.assign(relevantElement, existingCandidate)
  //
  //     _.remove(siteObject.image_candidates,
  //       candidate => candidate.tag_src == existingCandidate.tag_src)
  //
  //
  //     return siteObject.image_candidates.push(existingCandidate)
  //   }
  //
  //   // existingIncompleteCandidate only has metadata, i.e. the original
  //   // image has not been scraped yet or cannot be pulled from host's server
  //   const existingIncompleteCandidate = _.find(
  //     siteObject.incomplete_image_candidates,
  //     candidate => candidate.tag_src.includes(relevantElement.tag_src))
  //
  //   if (!existingIncompleteCandidate) {
  //     relevantElement.tag_src = $imageElement.attr('src')
  //     siteObject.incomplete_image_candidates.push(relevantElement)
  //     return true
  //   } else return false
  // }

  function isAbsoluteLink(siteUrl) {
    // console.log('\t' + chalk.bgMagenta('isAbsoluteLink') + chalk.magenta(` : PRINTPOINT AAA, siteUrl = ${chalk.blue(siteUrl)}, isAbsoluteLink = ${!!chalk.blue(siteUrl.match(/^(?:[a-z]+:)?\/\//i))}`))
    return siteUrl.match(/^(?:[a-z]+:)?\/\//i)
  }

  function getRelativeBucketLink(bucketName, linkUrl) {
    // e.g. url of web app is localhost:3030/s3/:bucket_name, without changing
    // these rel links the bucket version of site won't properly source assets

    // console.log('\t' + chalk.bgMagenta('getRelativeBucketLink') + chalk.magenta(` : PRINTPOINT AAA, linkUrl = ${chalk.blue(linkUrl)}, relativeBucketLink = ${chalk.blue(bucketName + (linkUrl[0] === '/' ? '' : '/') + linkUrl)}`))
    return '/s3/' + bucketName + (linkUrl[0] === '/' ? '' : '/') + linkUrl
  }

  externals.getRelativeBucketLink = function (bucketName, linkUrl) {
    return getRelativeBucketLink(bucketName, linkUrl)
  }

  function markElementAsRelevant($element, dataCategory, parentMarkerId) {
    // mark element for future reference and return uuid to submit to DB
    const relevantMarkerId = uuidV4().replace(/-/g, '')

    $element.attr('data-opm-marker-id', relevantMarkerId)
    $element.attr('data-opm-marker-category', dataCategory)

    if (parentMarkerId) {
      // if child already has data-opm-parent-marker-id,
      const existingId = $element.attr('data-opm-parent-marker-id')

      if (existingId && existingId !== parentMarkerId) {
        $element.attr('data-opm-parent-marker-id', existingId + ' ' + parentMarkerId)
      } else $element.attr('data-opm-parent-marker-id', parentMarkerId)
    }
    console.log('\t' + chalk.bgMagenta('markElementAsRelevant') +
      '\t' + $element.attr('data-opm-parent-marker-id') +
      '\t' + $element.attr('data-opm-marker-id') +
      '\t' + $element.attr('data-opm-marker-category'))
    return $element
  }

  function removeWhitespace(string) { return string.replace('\\n', '').replace('\\t', '').trim() }

  function searchForPhoneNumbers(elementText) {
    if (!elementText || !elementText.length) return null

    const matchResults = elementText.match(/\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})/g)
    if (matchResults && matchResults.length) return matchResults[0]
    else return null
  }

  function searchForEmails(elementText) {
    if (!elementText || !elementText.length) return null

    const matchResults = elementText.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi)
    if (matchResults && matchResults.length) return matchResults[0]
    else return null
  }

  function searchForAddresses(elementText) {
    if (!elementText || !elementText.length) return null

    const matchResults = elementText.match(/\s+(\d{2,5}\s+)(?![a|p]m\b)(([a-zA-Z|\s+]{1,5}){1,2})?([\s|\,|.]+)?(([a-zA-Z|\s+]{1,30}){1,4})(court|ct|street|st|drive|dr|lane|ln|road|rd|blvd)([\s|\,|.|\;]+)?(([a-zA-Z|\s+]{1,30}){1,2})([\s|\,|.]+)?\b(AK|AL|AR|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY)([\s|\,|.]+)?(\s+\d{5})?([\s|\,|.]+)/i)
    console.log(chalk.bgYellow('\t\t\t\n'))
    console.log('matchResults: '+ matchResults)
    console.log(chalk.bgYellow('\t\t\t\n'))
    if (matchResults && matchResults.length) return matchResults[0]
    else return null
  }

  return externals
}
