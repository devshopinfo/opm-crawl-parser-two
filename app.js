'use strict'

const _ = require('lodash')
const chalk = require('chalk')

const crawler = require('./crawler')
const db = require('./db')

db.query({
  // sql: 'SELECT * FROM n_sites LIMIT 10;',
  sql: 'SELECT * FROM n_sites WHERE site_id IN (1);',
  values: null,
  callback: async (error, results, fields) => {
    const parseResultsList = []

    if (error) return console.log(JSON.stringify(error, null, 4))

    else for (let row of results) {
      console.log('\treading a row! row.base_url = ' + chalk.blue(row.base_url))
      try {

        const crawlerResult = await crawler(row.base_url)
        if (typeof crawlerResult == 'object') {
          if (crawlerResult.exception ) {
            if (crawlerResult.exception == 'wp_reroute') {
              const newResult = await crawler(crawlerResult.new_route, true)
              parseResultsList.push(Object.keys(newResult))
            }
          } else parseResultsList.push(Object.keys(crawlerResult))

        }
      } catch (err) {
        console.log(chalk.bgRed('ERROR:') + chalk.red('\t' + err.stack))
      }

    }

    console.log(chalk.bgGreen('\t\t\t\t\t\t\t\t\t\t\t\t\t\n'))
    process.exit()
  }
})


// for testing without db
// const results = [
//   'http://www.bjjnewyorkcity.com/',
//   'http://www.marcelogarciajj.com/',
//   'http://www.selfdefenseschoolnewyork.com/',
//   'http://www.unityjiujitsu.com/',
//   'http://clockworkbjj.com/',
//   'http://www.jucaobjj.com/',
//   'http://www.gothamjiujitsu.com/',
//   'http://psabjj.com/',
//   'http://brooklynbjj.com/'
// ]
//
// async function fn1() {
//   const siteObjects = []
//   try {
//     for (let row of results) {
//       console.log('\treading a row! row = ' + chalk.blue(row))
//       siteObjects.push(await crawler(row))
//     }
//   } catch (err) {
//     console.log(chalk.bgRed('ERROR:') + '\t' + chalk.red(err))
//   }
//   siteObjects.join('\n')
//   console.log('\n\n\n' + chalk.bgGreen('FINAL:') + '\n' + siteObjects)
//   console.log(chalk.bgGreen('\t\t\t\t\t\t\t\t\t\t\t\t\t\n'))
// }
//
// fn1()
