const express = require('express')

const s3 = require('./aws')
const nunjucks = require('nunjucks')

const app = express()

app.use(express.static('assets'))

app.use('/s3/:bucket_name', async (req, res) => {
  console.log('Request received for S3 bucket, retrieving...')
  const fileName = req.path + (req.path.includes('.') ? '' : 'page.html')
  console.log(`Seeking file = ${fileName}, from bucket = ${req.params.bucket_name}`)

  try {
    const getFileResults = await s3.getSiteFile({
      bucket_name: req.params.bucket_name,
      s3_file_name: fileName
    })
    if (/\.(css)$/.test(fileName)) {
      res.writeHead(200, {'Content-Type': 'text/css'})
      res.write(getFileResults.Body) // <--- add this line
      res.end()
    } else if (/\.(js)$/.test(fileName)) {
      res.writeHead(200, {'Content-Type': 'text/javascript'})
      res.write(getFileResults.Body) // <--- add this line
      res.end()
    } else if (/\.(jpe?g)$/.test(fileName)) {
      res.writeHead(200, {'Content-Type': 'image/jpeg'})
      res.write(getFileResults.Body) // <--- add this line
      res.end()
    } else if (/\.(png)$/.test(fileName)) {
      res.writeHead(200, {'Content-Type': 'image/png'})
      res.write(getFileResults.Body) // <--- add this line
      res.end()
    } else {
      res.send(getFileResults.Body.toString())
    }
  } catch (err) {
    console.log('Oops! fileName = ' + fileName + '\n' + err)
    res.send(err)
  }
})

app.get('/s3', async (req, res) => {
  console.log('Default request received for S3 bucket, retrieving...')

  try {
    const bucketsList = []
    const listBucketsResults = await s3.listBuckets()

    if (!listBucketsResults.Buckets || !listBucketsResults.Buckets.length) {

      res.render('s3.directory.html', { buckets: bucketsList })

      throw Error('Error: no buckets found!')
    }

    for (let bucket of listBucketsResults.Buckets) {

      const bucketEntry = {
        name: bucket.Name,
        paths: []
      }

      const listObjectsResults = await s3.listObjects({
        bucket_name: bucket.Name
      })

      if (!listObjectsResults.Contents ||
          !listObjectsResults.Contents.length) continue

      for (let object of listObjectsResults.Contents) {

        if (object.Key.includes('.') &&
          !object.Key.includes('.html')) continue

        bucketEntry.paths.push(object.Key)
      }


      bucketsList.push(bucketEntry)
    }



    console.log('Success: list of buckets retrieved from S3 successfully')
    res.render('s3.directory.html', { buckets: bucketsList })

  } catch (err) {
    console.log('Oops!\n' + err)
    res.status(500).jsonp({ error: err })
  }
})

nunjucks.configure(
  [
    'server/templates',
    'server/templates/views'
  ], {
    autoescape: false,
    express: app,
    noCache: true
  }
)

console.log('App listening on port 3007...')
app.listen(3007)
